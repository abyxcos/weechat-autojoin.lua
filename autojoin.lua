--[[
Copyright (c) 2015, abyxcos@mnetic.ch

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--]]

--[[
TODO:
- Check PART format, does an empty part message end in :""?
- Simplify JOIN/PART regex
- Support keyed (password) channels

Dependencies:
- Weechat 1.2+
- Lua 5.1+
--]]

SCRIPT_NAME	= "autojoin"
SCRIPT_AUTHOR	= "<abyxcos@mnetic.ch>"
SCRIPT_VERSION	= "0.3"
SCRIPT_LICENSE	= "ISC"
SCRIPT_DESC	= "Automatically handle autojoin options"

-- Get (server,channel) and exit if messages aren't for us
function get_server_info(signal, signal_data, signal_test)
--	-- TODO: check PART format, does an empty message say :""?
	server = signal:match("(.+),")
	user,channel = signal_data:match(signal_test)

	-- Ignore JOIN messages not from us
	if weechat.info_get("irc_nick_from_host", signal_data) ~= weechat.info_get("irc_nick", server) then
		return nil,nil
	end

	-- Some servers prefix : before newly joined channels, in the form of :#channel
	channel = channel:gsub("^:", "")

	return server,channel
end

-- Write a table of channels to the server's autojoin list
function set_autojoin(server, autojoin_table)
	-- Join the table into a string
	autojoin_list = table.concat(autojoin_table, ",")
	-- Write the new autojoin list out
	weechat.command("", string.format(
		"/mute /set irc.server.%s.autojoin \"%s\"",
		server, autojoin_list
	))
	-- Save the config and the layout
	weechat.command("", "/mute /layout store")
	weechat.command("", "/mute /save irc")
end

-- Channel JOIN callback
function join_cb(data, signal, signal_data)
	server,channel = get_server_info(signal, signal_data, "(.+) JOIN (.+)")
	if server == nil or channel == nil then return weechat.WEECHAT_RC_OK end

	-- Grab the current autojoin list
	local autojoin_list = weechat.config_string(
		weechat.config_get("irc.server."..server..".autojoin")
	) or ""
	local autojoin_hash = {}

	-- Split the autojoin list into a hash
	for c in autojoin_list:gmatch("[^,]+") do
		autojoin_hash[c] = true
	end
	-- Add us to the hash (no worry about collisions)
	autojoin_hash[channel] = true

	-- Convert the autojoin hash to a table
	local autojoin_table = {}
	for k,v in pairs(autojoin_hash) do
		table.insert(autojoin_table, k)
	end

	set_autojoin(server, autojoin_table)
	return weechat.WEECHAT_RC_OK
end

-- Channel PART callback
function part_cb(data, signal, signal_data)
	server,channel = get_server_info(signal, signal_data, "(.+) PART (.-) :")
	if server == nil or channel == nil then return weechat.WEECHAT_RC_OK end

	-- Grab the current autojoin list
	local autojoin_list = weechat.config_string(
		weechat.config_get("irc.server."..server..".autojoin")
	) or ""
	local autojoin_table = {}

	-- Split the autojoin list into a table
	for c in autojoin_list:gmatch("[^,]+") do
		if c ~= channel then table.insert(autojoin_table, c) end
	end

	set_autojoin(server, autojoin_table)
	return weechat.WEECHAT_RC_OK
end

-- /autojoin command callback
function autojoin_cb(data, buffer, args)
	-- Iterate through the connected servers
	local server_list = weechat.infolist_get("irc_server", "", "")
	while weechat.infolist_next(server_list) > 0 do
		-- Iterate through the joined channels
		local server = weechat.infolist_string(server_list, "name")
		local autojoin_table = {}

		local channel_list = weechat.infolist_get("irc_channel", "", server)
		while weechat.infolist_next(channel_list) > 0 do
			local channel = weechat.infolist_string(channel_list, "name")
			table.insert(autojoin_table, channel)
		end

		set_autojoin(server, autojoin_table)
	end

	weechat.print("", "Joined channels updated.")
	return weechat.WEECHAT_RC_OK
end

weechat.register(
	SCRIPT_NAME,
	SCRIPT_AUTHOR,
	SCRIPT_VERSION,
	SCRIPT_LICENSE,
	SCRIPT_DESC,
	"", -- unload function
	""  -- charset
)
weechat.hook_signal("*,irc_in2_join", "join_cb", "")
weechat.hook_signal("*,irc_in2_part", "part_cb", "")
-- TODO: Check command name collisions?
weechat.hook_command(
	SCRIPT_NAME,
	"sync the autojoin lists",
	"sync",	-- args
	"Update the autojoin list to the current joined channels",
	"sync",	-- completion
	"autojoin_cb",
	""		-- data
)
